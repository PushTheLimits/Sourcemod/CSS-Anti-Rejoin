#include <sourcemod>
#include <morecolors>


// Plugin Info
#define AR_PLUGIN_VERSION "${-version-}" // Version is replaced by the GitLab-Runner compile script
#define AR_PLUGIN_NAME "CS:S  Anti-Rejoin"
#define AR_PLUGIN_AUTHOR "exvel, Kevin 'RAYs3T' Urbainczyk"
#define AR_PLUGIN_DESCRIPTION "Prevents players from beeing respawned after reconnecting."
#define AR_PLUGIN_WEBSITE "https://gitlab.com/PushTheLimits/Sourcemod/CSS-Anti-Rejoin"
#define AR_PREFIX_COLORED "{white}[{red}Anti-Rejoin{white}]{magenta}"


public Plugin myinfo =
{
	name = AR_PLUGIN_NAME,
	author = AR_PLUGIN_AUTHOR,
	description = AR_PLUGIN_DESCRIPTION,
	version = AR_PLUGIN_VERSION,
	url = AR_PLUGIN_WEBSITE
};


char PlayerClass[MAXPLAYERS + 1][4];
bool NeedClass[MAXPLAYERS + 1];
bool Played[MAXPLAYERS + 1];

Handle db = INVALID_HANDLE;

//CVars' handles
ConVar cvar_anti_rejoin;
ConVar cvar_lan;
ConVar cvar_deny_alive_rejoin;



public OnPluginStart()
{
	//Lets check what game is it and if it is not a CS:S return error
	char gameName[80];
	GetGameFolderName(gameName, 80);
	
	if (!StrEqual(gameName, "cstrike"))
	{
		SetFailState("This plugin is only for Counter-Strike: Source");
	}
	
	// Creating keyvalues. It is our DB.
	db = CreateKeyValues("antirejoin");
	
	// Creating cvars
	CreateConVar("sm_anti_rejoin_version", AR_PLUGIN_VERSION, "Anti-Rejoin Version", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	cvar_anti_rejoin = CreateConVar("sm_anti_rejoin", "1", "0 = disabled, 1 = slays players who play the round more than once", _, true, 0.0, true, 1.0);
	cvar_deny_alive_rejoin = CreateConVar("sm_anti_rejoin_deny_alive", "1", "0 = Prevent only dead players from respawning, 1 = Prevent even alive players from respawning", _, true, 0.0, true, 1.0);
	cvar_lan = FindConVar("sv_lan");

	
	// Hooking events
	HookEvent("round_end", ClearInfo, EventHookMode_Post);
	HookEvent("round_start", ClearInfo, EventHookMode_Pre);
	HookEvent("game_newmap", ClearInfo, EventHookMode_Post);
	HookEvent("player_death", Event_PlayerDeath, EventHookMode_Post);
	HookEvent("player_spawn", Event_PlayerSpawn, EventHookMode_Post);
	
	// This is for our punishment
	RegConsoleCmd("joinclass", Command_JoinClass);
}

// Here we can control our punishment. If player already played in this round
// he will not spawn because we blocked "joinclass" command
public Action Command_JoinClass(client, args)
{
	char steamId[30];
	GetClientAuthId(client, AuthId_SteamID64, steamId, sizeof(steamId));
		
	if (GetClientTeam(client) == 1 || KvGetNum(db, steamId, 0) == 0 || IsFakeClient(client) || GetConVarBool(cvar_lan) || !GetConVarBool(cvar_anti_rejoin))
	{
		NeedClass[client] = false;
		return Plugin_Continue;
	}
	
	if (args == 0)
	{
		PlayerClass[client] = "0";
		NeedClass[client] = true;
	}
	else
	{
		GetCmdArg(1, PlayerClass[client], sizeof(PlayerClass));
		NeedClass[client] = true;
	}
	
	FakeClientCommandEx(client, "spec_mode");
	
	CPrintToChat(client, "%s Sorry, no luck for you. High after retry is not supported here >.>", AR_PREFIX_COLORED);
	
	return Plugin_Handled;
}

// If player respawned somehow (by other plugin for example) we will not punish him
public Action Event_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	char steamId[30];
	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	GetClientAuthId(client, AuthId_SteamID64, steamId, sizeof(steamId));
	
	KvDeleteKey(db, steamId);
	NeedClass[client] = false;
	Played[client] = false;
}

// If player died it means that he already played and if he will disconnect we can add his steamId to DB
public Action Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
		
	if (GetConVarBool(cvar_lan) || IsFakeClient(client))
	{
		return Plugin_Continue;
	}
	
	Played[client] = true;
	
	return Plugin_Continue;
}

// On each round start/end or new map start we must clear DB and all info
public Action ClearInfo(Handle event, const char[] name, bool dontBroadcast)
{
	CloseHandle(db);
	db = CreateKeyValues("antirejoin");
	
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client) && GetClientTeam(client) != 1 && NeedClass[client])
		{
			// Ok, now we can spawn poor player. DB was cleared so "joinclass" command is available
			FakeClientCommandEx(client, "joinclass %s", PlayerClass[client]);
		}
		
		NeedClass[client] = false;
		Played[client] = false;
	}

	return Plugin_Continue;
}

// Just for sure
public OnClientPutInServer(client)
{
	NeedClass[client] = false;
	Played[client] = false;
}

// If player already played we will add him to DB
public OnClientDisconnect(client)
{
	if (GetConVarBool(cvar_lan) || !GetConVarBool(cvar_anti_rejoin) || IsFakeClient(client))
	{
		return;
	}
	
	// Check if we have to prevent any player from rejoin or just alive ones
	if (GetConVarBool(cvar_deny_alive_rejoin) || Played[client])
	{
		char steamId[30];
		GetClientAuthId(client, AuthId_SteamID64, steamId, sizeof(steamId));
		
		KvSetNum(db, steamId, 1);
	}
}