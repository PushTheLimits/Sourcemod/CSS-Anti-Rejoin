# Sourcemod CS:S Anti-Rejoin Plugin
Prevents players from beeing respawned if they reconnect.

## Forked version
this is a new version of the Anti-Rejoin Plugin, forked from [exvel](exvel)'s [Anti-Rejoin Plugin](https://forums.alliedmods.net/showpost.php?p=662510&postcount=1)

This fork has 
* Added the option to also prevent players to respawn which were alive at the time they reconnected
* Changed the code style to the modern one.
* Optimized some code fragments


## Installation

TODO


## Configuration

if you just want to prevent dead players from beein respawned set `sm_anti_rejoin` to true (default).

If you also wish to punish alive players which are reconnecting set `sm_anti_rejoin_deny_alive` also to true (default)